<h3>Uitslag</h3>
<p>
    Hieronder zie je de score, per partij zie je hoeveel punten je gescoord hebt. Elk antwoord dat jij hebt gegeven is
    een bepaald aantal punten per partij waard. De worden bij elkaar opgeteld. De partij met de de meeste punten komt
    het beste overeen met je antwoorden.
</p>
<p>
    Ben je benieuwd wat voor invloed andere antwoorden hebben op je score, maak dan de test gerust opnieuw.
</p>

<div class="table-responsive">
    <table class="table table-striped table-bordered" id="results">
        <thead>
        <tr>
            <td></td>
            @foreach($partijen as $partij)
                <td>{!! $partij->name !!}</td>
            @endforeach
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Punten</td>
            @foreach($partijen as $partij)
                <td id="partij-{!! $partij->id !!}" class="points">0</td>
            @endforeach
        </tr>
        </tbody>
    </table>
</div>

<div id="socialMedia">

    <div class="col-xs-12">
        <a href="javascript:history.go(0)" class="btn btn-social btn-green"><i class="fa fa-refresh"></i><span>Doe de test opnieuw</span></a>
    </div>

    <div class="col-xs-12">
        <a class="btn btn-social btn-facebook"
           href="https://www.facebook.com/sharer/sharer.php?sdk=joey&u=http%3A%2F%2Fstemwijzer.routestemmer.nl%2F&display=popup&ref=plugin&src=share_button"
           onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
            <i class="fa fa-facebook"></i><span>Delen op Facebook</span>
        </a>
    </div>

    <div class="col-xs-12">
        <a class="btn btn-social btn-twitter"
           href="https://twitter.com/intent/tweet?hashtags=watstemjij%2Croutestemmer&ref_src=twsrc%5Etfw&text=Bij%20welke%20partij%20voel%20jij%20je%20thuis%3F%20Doe%20de%20Routestemmer%20en%20je%20weet%20het.&tw_p=tweetbutton&url=http%3A%2F%2Fstemwijzer.routestemmer.nl"
           onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
            <i class="fa fa-twitter"></i><span>Delen op Twitter</span>
        </a>
    </div>

    <div class="col-xs-12">
        <a class="btn btn-social btn-google"
           href="https://plus.google.com/share?url=http://stemwijzer.routestemmer.nl"
           onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
            <i class="fa fa-google"></i><span>Delen op Google+</span>
        </a>
    </div>
</div>
@extends('layout.default')

@section('title')
    Routestemmer
@stop

@section('content')

    <?php
    $i = 0;
    $x = 0;

    $JSindexArray = \App\Index::all();
    $JSpartijen = \App\Partij::select('id')->get();
    $questions = $questions->reverse();

    $x = $questions->count();

    echo "<script>\n";
    echo 'var questions = ' . $x . ' + 2;';
    echo 'var index = ' . json_encode($JSindexArray, JSON_PRETTY_PRINT) . ';';
    echo 'var partijen = ' . json_encode($JSpartijen, JSON_PRETTY_PRINT) . ';';
    echo "\n</script>";

    ?>

    <div class="container" id="container">
        <div id="box{!! $x+3 !!}" class="box">
            @include('table')
        </div>

        <div id="box{!! $x+2 !!}" class="box">
            <div>
                <h3>Vraag {!! $x+1 !!}/{!! $questions->count()+1 !!}</h3>
                <h4>
                    Als partij in de regering heb je directe invloed op het beleid (ook al moeten compromissen
                    gesloten worden ten koste van eigen standpunten).<br/><br/>

                    De meeste grote partijen streven naar directe invloed. Andere partijen vinden het belangrijker
                    om vast te houden aan eigen standpunten en proberen vanuit de oppositie invloed te krijgen.
                </h4>

                <p>Welke drie partijen moet volgens jou in elk geval in een nieuwe regering?</p>
            </div>

            <div class="answer-group">
                @for($i = 1; $i <= 3; $i++)
                    <div class="form-group">
                        <input type="hidden" value="0">
                        <select class="form-control">
                            <option disabled selected>-- Kies een partij --</option>

                            @foreach($partijen as $partij)
                                <option value="{!! $partij->id !!}">{!! $partij->name !!}</option>
                            @endforeach
                        </select>
                    </div>
                @endfor
            </div>

            <div class="next-wrapper">
                <button class="btn btn-orange btn-block next" id="next" disabled>Bekijk de uitslag</button>
            </div>
        </div>

        @foreach($questions as $question)

            <div id="box{!! $x+1 !!}" class="box">
                <?php dd($question) ?>


                <div>
                    <h3>Vraag {!! $x !!}/{!! $questions->count()+1 !!}</h3>
                    <div class="question @if($question->question->collapse)tw-content @endif">{!! $question->question->question !!}</div>
                    @if($question->question->collapse)
                        <a class="tw-more">Toon <span>meer</span></a>
                    @endif

                    <input type="hidden" value="0">
                    <?php
                    $answers_id = $question->question->answers_id;
                    $answers_id = explode(',', $answers_id);
                    ?>
                    <div class="answer-group" data-toggle="buttons">
                        @foreach($answers_id as $answers)
                            <?php $answers = \App\Answers::select('*')->where('id', '=', $answers)->get(); ?>

                            @foreach($answers as $answer)

                                <label class="btn btn-green btn-block">
                                    <input class="radio" type="radio"
                                           id="question-{!! $question->question->id !!}-answer-{!! $answer->id !!}"
                                           name="optionsRadios{!! $i !!}"
                                           value="{!! $answer->points !!}">
                                    {!! $answer->answer !!}
                                </label>
                            @endforeach {{-- END $answers as $answer --}}
                        @endforeach {{-- END $answers_id as $answers --}}
                    </div>

                    <div class="next-wrapper">
                        <button class="btn btn-orange btn-block next" disabled>Volgende vraag</button>
                    </div>
                </div>

            </div>

            <?php
            $x--;
            ?>
        @endforeach

        <div id="box1" class="box ativo">
            <div>
                <h3>Routestemmer</h3>
                <h4>Bij welke partij voel jij je thuis? Doe de Routestemmer en je weet het.</h4>
                <h4>Deze test bestaat uit <?= $questions->count() + 1; ?> vragen over actuele onderwerpen.</h4>
                <div class="next-wrapper">
                    <button class="btn btn-orange btn-block button next" id="start">Start de routestemmer</button>
                </div>
            </div>
        </div>

    </div>
    <div id="progress-bar"></div>

@stop

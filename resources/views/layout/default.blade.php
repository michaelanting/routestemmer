@include('layout.header')

@include('layout.banner')

@yield('content')

@include('layout.footer')

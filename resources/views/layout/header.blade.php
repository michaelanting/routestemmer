<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Routestemmer - @yield('title')</title>

    <meta name="robots" content="noindex, nofollow"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="theme-color" content="#D1EAE0">

    <link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet" type="text/css">

    <script src="http://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript"></script>

    {!! Html::style('style/bootstrap/bootstrap.min.css') !!}
    {!! Html::style('style/bootstrap/bootstrap-switch.css') !!}
    {!! Html::style('style/main.css') !!}

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>

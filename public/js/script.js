$("input:radio").change(function () {
    $(this).parent().parent().parent().find('.next').prop('disabled', false);
    $(this).parent().parent().parent().find('.prev').prop('disabled', false);

    var partijen = [];                                                                  // Create partijen Array, needs to be declared early
    var old_partijen = $(this).parent().parent().parent().find("input:hidden");         // Get partijen from hidden input
    var old_partijen_array = old_partijen.val().split(',');                             // Push partijen to Array
    var value = $(this).val();                                                          // Get score value from Radio button

    var id = $(this).prop('id');                                                        // Get question and answer from Block Element ID
    id = id.split('-');                                                                 // Split the string into an Array

    var question_id = id[1];                                                            // Get question_id
    var answer_id = id[3];                                                              // Get answer_id

    var result = $.grep(index, function (e) {
        return e.answer_id == answer_id && e.question_id == question_id;                // Find items from Index DB Table (var index)
    });

    // Remove old scores from table
    if (old_partijen_array[0] != 0) {
        for (var x = 0; x < old_partijen_array.length; x++) {
            var partij = document.getElementById('partij-' + old_partijen_array[x]);
            var current_score = parseInt(partij.innerHTML);

            updateScore(partij, current_score, -value);
        }
    }

    // Add new scores to table
    for (var x = 0; x < result.length; x++) {
        var partij = document.getElementById('partij-' + result[x].partij_id);
        var current_score = parseInt(partij.innerHTML);

        partijen.push(result[x].partij_id);                                             // Push every partij to the Array

        if (current_score == 0) {
            if (value != -2) {
                updateScore(partij, current_score, value);
            }
        } else {
            updateScore(partij, current_score, value);
        }
    }

    partijen = partijen.join();                                                         // Make a string from currently scoring partijen
    old_partijen.val(partijen);                                                         // Save above string to hidden input

});


$("select").change(function () {
    $(this).parent().parent().parent().find('.next').prop('disabled', false);

    var partijen = [];                                                                  // Create partijen Array, needs to be declared early
    var old_partijen = $(this).parent().find("input:hidden");                           // Get partijen from hidden input
    var old_partijen_array = old_partijen.val().split(',');                             // Push partijen to Array
    var value = $(this).val();                                                          // Get score value from Radio button

    // Remove old scores from table
    if (old_partijen_array[0] != 0) {
        for (var x = 0; x < old_partijen_array.length; x++) {
            var partij = document.getElementById('partij-' + old_partijen_array[x]);
            var current_score = parseInt(partij.innerHTML);

            updateScore(partij, current_score, -2);
        }
    }

    // Add new scores to table
    var partij = document.getElementById('partij-' + value);
    var current_score = parseInt(partij.innerHTML);

    partijen.push(value);                                                               // Push every partij to the Array

    updateScore(partij, current_score, 2);

    partijen = partijen.join();                                                         // Make a string from currently scoring partijen
    old_partijen.val(partijen);                                                         // Save above string to hidden input
})
;

function updateScore(partij, current_score, value) {
    partij.innerHTML = '';
    partij.innerHTML = current_score + parseInt(value);
}

$(function () {
    $(".next").click(function () {
        updatecolors();
        updateProgressBar();

        var obj = $(".ativo");
        var prox = $(obj).siblings(":last");
        $(obj).animate({
            right: '+150%'
        }, 500, function () {
            $(prox).prependTo('#container');
        });
        $(prox).css('right', '-50%');
        $(prox).animate({
            right: '+50%'
        }, 500, function () {

            $(this).addClass('ativo');
            $(obj).removeClass('ativo');
            $('#box1').remove();
        });
    });
});

var updatecolors = function () {
    var cells = document.getElementsByClassName('points');
    for (var i = 0, len = cells.length; i < len; i++) {
        if (parseInt(cells[i].innerHTML, 10) < 1) {
            $(cells[i]).addClass('danger');
        }
        else if (parseInt(cells[i].innerHTML, 10) >= 1 && parseInt(cells[i].innerHTML, 10) <= 4) {
            $(cells[i]).addClass('warning');
            $(cells[i]).removeClass('danger');
        } else {
            $(cells[i]).addClass('success');
            $(cells[i]).removeClass('warning');
        }
    }
};

var updateProgressBar = function () {
    var progressBar = $('#progress-bar');
    var addWidth = 100 / questions;

    var width = progressBar.width();
    var parentWidth = $('body').offsetParent().width();
    var percent = 100 * width / parentWidth;

    progressBar.css('width', percent + addWidth + '%');
};

$('.tw-more').click(function () {
    var parent = $(this).parent().find('.tw-content');
    $(this).find('span').html('minder');

    if (parent.hasClass('collapsed-content')) {
        $(this).find('span').html('meer');
    }

    parent.toggleClass('collapsed-content');
});
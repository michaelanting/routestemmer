<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Index extends Model
{
    protected $table = 'answer_points';

    protected $fillable = ['question_id', 'answer_id', 'partij_id', 'points'];

    public function answer() {
        return $this->belongsTo(Answers::class);
    }

    public function question() {
        return $this->belongsTo(Questions::class);
    }

    public function partij() {
        return $this->belongsTo(Partij::class);
    }
}
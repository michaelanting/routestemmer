<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Input;
use Redirect;
use Validator;
use App\User;
use App\Questions;
use App\Partij;
use App\Answers;
use App\Index;
use App\Http\Controllers\Controller;

class GuideController extends Controller
{

    public function __construct() {
        return View('index', ['questions' => $this->getAnswers(), 'partijen' => $this->getPartijen()]);
    }

    private function getQuestions() {
        return Questions::select('*')->sortBy('collapse', 'DESC')->get();
    }

    private function getAnswers() {
//         return Index::select('*')->where('question_id', '>', 50)->groupBy('question_id')->get();
       return Index::select('*')->groupBy('question_id')->get();
    }

    private function getPartijen() {
        return Partij::all();
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
    protected $table = 'questions';

    protected $fillable = ['question', 'author_id'];

    public function index() {
        return $this->hasMany(Index::class);
    }
}

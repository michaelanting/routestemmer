<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partij extends Model
{
    protected $table = 'partijen';

    protected $fillable = ['name', 'lijsttrekker'];

    public function index() {
        return $this->hasMany(Index::class);
    }
}

module.exports = function (grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    less: {
      development: {
        options: {
          paths: ['public/style/less'],
          yuicompress: false,
          livereload: true
        },
        files: {
          'public/style/main.css': [
            'public/style/less/mobile/main.less',
            'public/style/less/desktop/main.less'
          ]
        }
      }
    },
    cssmin: {
      compress: {
        files: {
          'public/style/main.min.css': ['public/style/main.css']
        }
      }
    },
    watch: {
      less: {
        files: [
          'public/style/less/*.less',
          'public/style/less/mobile/*.less',
          'public/style/less/desktop/*.less'
        ],
        tasks: ['less']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['less', 'cssmin']);

};
